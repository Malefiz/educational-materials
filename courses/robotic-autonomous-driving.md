# Status: Draft

# Scenario Description
To autonomously drive a vehicle in general, Convolutional Neural Networks are commmonly used [[LEC98]](#lec98). A research team has proven, that a complex architecture is capable to drive a car by using an end-to-end concept [[NVI16]](#nvi16). Therefor a paradigm called Imitation Learning has been applied [[HUS17]](#hus17). Hereby, the model is trained supervised and the action is mimiced.
<br>
Furthermore, another paradigm, Reinforcement Learning [[SB98]](#sb98) is eligible. In Reinforcment Learning, a reward system is required for training instead of labeled data. Based on this approach, researchers sucessfully trained models, which are capable of solving different complex tasks [[KOR13]](#kor13), also in the continuous action space [[SIL14]](#sil14)[[LIL15]](#lil15). 


# Robotic / Autonomous Driving

<table>
    <tr width="100%">
        <td width="49%" align="center">
                <img src="../media/baumann/prototype_v2_rev2_mirrored_cut.jpg" alt="Prototype v2 rev2">
        </td>
        <td width="51%" align="center">
                <img src="../media/baumann/prototype_v2_rev2_cut.jpg" alt="Prototype v2 rev2">
        </td>
    </tr>
</table>

<center>(Images: CC-BY-SA 4.0 Bruno Schilling / HTW Berlin, 2018)</center>
<br><br>
Based on the Supervised Learning approach a real word prototype racecar in scale 1:10 is built and equipped with a camera. Data can be recorded to train a model, capable of driving autonomously on a random track with pylons as borders (pylons are also in scale 1:10). Further sensors and different cameras can be applied to the real world prototype as replacement or in addition to the used camera.  
With the paradigm of Reinforcement Learning in mind, a simulator <a href="#kh04">[KH04]</a> in combination with the Robot Operating System <a href="#qui09">[QUI09]</a> and OpenAI Gym <a href="#bro16">[BRO16]</a> is used, to provide the possibility of training networks by different reward functions. The <a href="https://github.com/mit-racecar">MIT-Racecar</a> project is used as base for the simulated version of the real world prototype.  
The design of the implementation is split into standalone modules. This architecture allows to train a model by Imitation Learning in real world or a simulated environment and afterwards, to improve it further with Reinforcement Learning. As the basic system, Linux in combination with ROS is used.
<br><br>
For further details see
<br><br>

* [Wiki](https://gitlab.com/NeuroRace/neuroracer-documentation/wikis/Home)
* [Gitlab Sources](https://gitlab.com/NeuroRace?sort=name_asc)

<!--
# Teaching Material

Content for machine learning tasks regarding robotic and autonomous control, realized as Jupyter notebooks.

(*in progress*)
* Localization / SLAM (Simultaneous Localization and Mapping)
* Behaviour Cloning (Supervised Learning)
* (Deep) Reinforcement Learning (Model-free)
-->

## References

<table>
    <tr>
        <td>
            <a name="bro16"></a>[BRO16]
        </td>
        <td>
            Brockman, Greg, et al. "OpenAI Gym." CoRR abs/1606.01540.
        </td>
    </tr>
    <tr>
        <td>
            <a name="hus17"></a>[HUS17]
        </td>
        <td>
            Hussein, Ahmed, et al. "Imitation Learning: A Survey of Learning Methods." ACM Comput Surv 50.2, 21:1-21:35.
        </td>
    </tr>
    <tr>
        <td>
            <a name="kh04"></a>[KH04]
        </td>
        <td>
            Koenig, Nathan and Howard, Andrew. "Design and use paradigms for Gazebo, an open-source multi-robot simulator." Proceedings of 2004 IEEE/RSJ IROS.
        </td>
    </tr>
    <tr>
        <td>
            <a name="kor13"></a>[KOR13]
        </td>
        <td>
            Mnih, Volodymyr and Kavukcuoglu, Koray, et al. "Playing Atari with Deep Reinforcement Learning." CoRR abs/1312.5602.
        </td>
    </tr>
    <tr>
        <td>
            <a name="lec98"></a>[LEC98]
        </td>
        <td>
            LeCun, Yann, et al. "Gradient-based learning applied to document recognition." Proceedings of the IEEE 86.11 (1998): 2278-2324.
        </td>
    </tr>
    <tr>
        <td>
            <a name="lil15"></a>[LIL15]
        </td>
        <td>
            Lillicrap, Timothy, et al. "Continuous control with deep reinforcement learning." CoRR abs/1509.02971.
        </td>
    </tr>
    <tr>
        <td>
            <a name="nvi16"></a>[NVI16]
        </td>
        <td>
            LeCun, Yann, et al. "Gradient-based learning applied to document recognition." Proceedings of the IEEE 86.11 (1998): 2278-2324.
        </td>
    </tr>
    <tr>
        <td>
            <a name="qui09"></a>[QUI09]
        </td>
        <td>
            Quiqley, Morgan, et al. "ROS: an open-source Robot Operating System." ICRA workshop on open sourc software. Vol. 3. 3.2. 2009.
        </td>
    </tr>
    <tr>
        <td>
            <a name="sb98"></a>[SB98]
        </td>
        <td>
            Sutton, Richard and Barto, Andrew "Introduction to Reinforcement Learning." 1st Cambridge, MA, USA: MIT Press, 1998.
        </td>
    </tr>
    <tr>
        <td>
            <a name="sil14"></a>[SIL14]
        </td>
        <td>
            Silver, David, et al. "Deterministic Policy Gradient Algorithms." Proceedings of the 31st International Conference on Machine Learning (ICML-14): 387-395.
        </td>
    </tr>
</table>
