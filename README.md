# deep.TEACHING Website and Notebooks

This repositories contains Markdown source files for the [deep.TEACHING website](https://www.deep-teaching.org/) and Jupyter notebooks with teaching materials. The repository is meant to be self contained, allowing for offline learning and editing, as well as easy distribution.

Open [index.md](index.md) and start exploring the teaching materials.

## Acknowledgements

The deep.TEACHING notebooks are developed at HTW Berlin - University of Applied Sciences.<br/>
The work is supported by the German Ministry of Education and Research (BMBF).
.
