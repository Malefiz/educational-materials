{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Estimators: Fundamentals and Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Knowledge](#Knowledge) \n",
    "  * [Modules](#Python-Modules)\n",
    "* [Exercise: Pen and Paper Calculation](#Pen-and-Paper-Calculation)\n",
    "* [Exercise: Visualization of the Column and Null Space](#Implementation-of-Basic-Operations)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)\n",
    "* [Literature](#Literature) \n",
    "* [Licenses](#Licenses) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "This notebook describes some fundamental properties of estimators, such as bias, variance and mean squared error.\n",
    "The exercises help the reader to understand the topics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "As requirements you need fundamental knowledge about probabilities.\n",
    "\n",
    "\n",
    "It's recommend to study the videos on Khan academy about [sampling distributions](https://www.khanacademy.org/math/ap-statistics/sampling-distribution-ap/what-is-sampling-distribution/v/introduction-to-sampling-distributions).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fundaments of Estimators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Data\n",
    "\n",
    "$$\n",
    "\\mathcal D = \\{ X_1, X_2, \\dots X_n\\}\n",
    "$$\n",
    "\n",
    "Here we consider the data as random variables (frequentist view).\n",
    "\n",
    "Also, the Data is iid (independent and identically distributed):\n",
    "\n",
    "- independent: the outcome of one observation does \n",
    "    not effect the outcome of another observation\n",
    "- identically distributed, i.e. all $X_i$ are drawn from the same probability distribution. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Definition of a statistic\n",
    "\n",
    "A __statistic__ is a random variable $S$ that is a function of the data $\\mathcal D$, i.e. \n",
    "$$\n",
    "S = f \\left(\\mathcal D \\right)\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "An __estimator__ is a statistic attended to approximate a parameter governing the distribution of the data $\\mathcal D$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Notation: $\\hat \\theta$ is an estimator of $\\theta$\n",
    "<!-- 1. $\\hat \\theta_n$ is an estimator of $\\theta$ -->"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Bias of an estimator\n",
    "\n",
    "The __bias__ of an estimator $\\hat \\theta$ is \n",
    "$$\n",
    "\\text{bias}(\\hat \\theta) := \\mathbb E\\left[ \\hat \\theta \\right] - \\theta\n",
    "$$\n",
    "Difference between mean estimated value and the true value of $\\theta$. \n",
    "\n",
    "The expectation (mean) is with respect to (w.r.t.) to different data sets $\\mathcal D$. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "An estimator is __unbiased__ if the bias is zero, i.e. \n",
    "$$\\text{bias}(\\hat \\theta)= 0$$ \n",
    "or\n",
    "$$\n",
    "\\mathbb E\\left[ \\hat \\theta \\right] = \\theta\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Variance of an estimator\n",
    "\n",
    "\"How much does the estimated value change for different data sets.\"\n",
    "\n",
    "$$\n",
    "var(\\hat \\theta) = \\mathbb E \\left[\\left(\\mathbb E [\\hat \\theta] - \\hat \\theta\\right)^2\\right]\n",
    "$$\n",
    "\n",
    "or\n",
    "\n",
    "\\begin{align}\n",
    "var(\\hat \\theta) \n",
    " &= \\mathbb E \\left[\\left(\\mathbb E [\\hat \\theta] - \\hat \\theta\\right)^2\\right] \\\\\n",
    " &= \\mathbb E \\left[(\\mathbb E [\\hat \\theta])^2 -2\\mathbb E [\\hat \\theta] \\hat \\theta+ \\hat \\theta^2\\right] \\\\\n",
    " &= (\\mathbb E [\\hat \\theta])^2 - 2 \\mathbb E [\\hat \\theta]\\mathbb E [\\hat \\theta] + \\mathbb E [\\hat \\theta^2] \\\\\n",
    " &= \\mathbb E [\\hat \\theta^2]  - (\\mathbb E [\\hat \\theta])^2\n",
    "\\end{align}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Example\n",
    "\n",
    "Data generating distribution is a univariate Gaussian:\n",
    "\n",
    "$$\n",
    "X_i \\sim \\mathcal N(\\mu, \\sigma^2)\n",
    "$$\n",
    "\n",
    "The Gaussian has two parameters which we want to estimate from the data:\n",
    " - Mean: $\\mu = \\mathbb E \\left[ X\\right]$\n",
    " - Variance of the Gaussian $\\sigma^2 = \\mathbb E \\left[ (X - \\mu)^2 \\right]$\n",
    " \n",
    "Note that the technical term variance is used here for two different concepts\n",
    "- variance of the estimator $var(\\hat \\theta)$ and \n",
    "- variance of the Gaussian $\\sigma^2$.\n",
    "\n",
    " \n",
    "We can use the following estimators of $\\mu$ and $\\sigma^2$:\n",
    "\n",
    "- Sample mean is an estimator of the mean $\\mu$:     \n",
    "$$\\hat \\mu = \\frac{1}{n} \\sum_{i=1}^n X_i =: \\bar X  $$\n",
    "\n",
    "- (Biased) sample variance is an estimator of the variance $\\sigma^2$:     \n",
    "$$\\hat \\sigma_b^2 = \\frac{1}{n}\\sum_{i=1}^n (X_i- \\bar X)^2$$\n",
    "\n",
    "- (Unbiased) sample variance is an estimator of the variance $\\sigma^2$:     \n",
    "$$\\hat \\sigma_u^2 = \\frac{1}{n-1} \\sum_{i=1}^n (X_i- \\bar X)^2$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Estimator $\\hat \\mu$ is unbiased:\n",
    "\n",
    "$$\n",
    "\\mathbb E \\left[ \\hat \\mu \\right] = \\mathbb E \\left[ \\frac{1}{n} \\sum_i X_i \\right]  \n",
    "= \\frac{1}{n} \\sum_i   \\mathbb E \\left[ X_i\\right] = \\frac{1}{n} \\sum_i  \\mu = \\mu\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variance of the estimator $\\hat \\mu$\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "var(\\hat \\mu) & := \\mathbb E \\left[ (\\hat \\mu  - \\mathbb E [\\hat \\mu])^2 \\right]\\\\\n",
    "&=\\mathbb E \\left[ (\\bar X - \\mu)^2 \\right] \\\\\n",
    "&=\\mathbb E \\left[ \\left(1/n \\sum_i X_i - \\mu\\right)^2 \\right] \\\\\n",
    "& =\\mathbb E \\left[ \\left(1/n \\sum_i X_i\\right)^2 - 2\\mu /n \\sum_i X_i + \\mu ^2 \\right] \\\\\n",
    "& = \\mathbb E \\left[ \\bar X^2 \\right] - 2 \\mu/n \\sum_i \\mathbb E \\left[ X_i \\right] + \\mu^2\\\\ \n",
    "& = \\mathbb E \\left[ \\bar X^2 \\right] - 2 \\mu \\mu  + \\mu^2\\\\ \n",
    "& = \\mathbb E \\left[ \\bar X^2 \\right] - \\mu^2\\\\ \n",
    "\\end{align}\n",
    "$$ \n",
    "\n",
    "This derivation was analog to the derivation of the alternative definition of the variance.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a more in depth discussion of the example and \n",
    "the bias and variance of the estimators $\\hat \\sigma_b^2$ and $\\hat \\sigma_u^2$, see\n",
    "\n",
    "- https://onlinecourses.science.psu.edu/stat414/node/166\n",
    "- https://onlinecourses.science.psu.edu/stat414/node/192\n",
    "\n",
    "\n",
    "<!--\n",
    "Variance of $\\sigma_b^2$:\n",
    "\n",
    "Note:\n",
    "$$\n",
    "\\begin{align}\n",
    "\\hat \\sigma_b^2 & = \\frac{1}{n}\\sum_{i=1}^n \\left(X_i- \\bar X\\right)^2 = \\frac{1}{n}\\sum_{i=1}^n \\left(X_i^2 - 2 X_i \\bar X+ \\bar X^2\\right) =\n",
    " \\frac{1}{n}\\sum_{i=1}^n X_i^2 - \\frac{1}{n}\\sum_{i=1}^n \\left( 2 X_i \\bar X \\right) + \\bar X^2 \\\\\n",
    " & = \\frac{1}{n}\\sum_{i=1}^n X_i^2 - 2 \\bar X^2  + \\bar X^2 = \\frac{1}{n}\\sum_{i=1}^n X_i^2 - \\bar X^2  \n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "and with the alternative definition of the variance:\n",
    "$$\n",
    "Var(X) = \\sigma^2 = \\mathbb E \\left[ X^2 \\right] - \\mu^2 =\\frac{\\sigma^2}{n} \n",
    "$$\n",
    "-->\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Mean squared error of an estimator\n",
    "\n",
    "$$\n",
    "mse(\\hat \\theta) = \\mathbb E \\left[ \\left(\\theta - \\hat \\theta ]\\right)^2 \\right]\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Relation between mean squared error, bias and variance\n",
    "\n",
    "Bias-Variance decomposition of the mean squared error:\n",
    "\n",
    "$$\\begin{align}\n",
    "mse(\\hat \\theta) &= \\mathbb E \\left[ \\left(\\theta - \\hat \\theta ]\\right)^2 \\right] \\\\\n",
    "  &= \\mathbb E [ \\theta^2] - 2 \\mathbb E[\\theta \\hat \\theta] + \\mathbb E[\\hat \\theta^2]\\\\\n",
    "  &= \\theta^2 - 2 \\mathbb \\theta \\mathbb E [\\hat \\theta] + \\mathbb E[\\hat \\theta^2] + (\\mathbb E [\\hat \\theta])^2 - (\\mathbb E [\\hat \\theta])^2\\\\\n",
    "  &= \\left(\\theta^2 - 2 \\mathbb \\theta \\mathbb E [\\hat \\theta]  + (\\mathbb E [\\hat \\theta])^2  \\right) + \\left( \\mathbb E[\\hat \\theta^2] - (\\mathbb E [\\hat \\theta])^2 \\right)\\\\\n",
    "  &=\\left(\\mathbb E\\left[ \\hat \\theta \\right] - \\theta  \\right)^2  + var(\\hat \\theta) \\\\\n",
    "  &= \\left(\\text{bias}(\\hat \\theta)\\right)^2 + var(\\hat \\theta) \n",
    "\\end{align}$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sufficient Statistics\n",
    "\n",
    "A statistic $S$ is sufficient if\n",
    "\n",
    "$$\n",
    "p(\\hat \\theta \\mid \\mathcal D) = p\\left(\\hat \\theta \\mid S(\\mathcal D)\\right)\n",
    "$$\n",
    "\n",
    "For estimation of the parameters all information is \"compressed\" in the sufficient statistic. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example\n",
    "\n",
    "We have a bowl with enumerated balls from 1 to 100 (population).\n",
    "\n",
    "As sample we draw 5 balls (i.e. five numbers) with replacement.\n",
    "\n",
    "A sampling distribution is the distribution of a statistic (here  $\\mu_1$), that is obtained by repeatedly drawing a large number of samples from a specific population. This distribution allows you to determine the probability of obtaining the sample statistic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "population = np.arange(1,101,1, dtype=np.int32) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "true_median = np.median(population)\n",
    "true_median # unknown - we want to estimate this from a sample"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q75, q25 = np.percentile(population, [75 ,25])\n",
    "true_IQR = q75 - q25\n",
    "true_IQR # unknown - we want to estimate this from a sample"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want decide:\n",
    "- Is the median of a sample an unbiased estimator of the median of the population?\n",
    "- Is the IQR (inter quatile range) of a sample an unbiased estimator IQR of the population?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "samples = []\n",
    "for i in range(10000): # without loop?\n",
    "    samples.append(np.random.choice(population, size=(5), replace=True))\n",
    "\n",
    "samples = np.array(samples)\n",
    "sample_distribution_median = np.median(samples, axis=1)\n",
    "q75, q25 = np.percentile(samples, [75 ,25], axis=1)\n",
    "sample_distribution_inter_quartile_range = q75 -q25"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10,8))\n",
    "plt.title(\"approximated sample distribution\")\n",
    "plt.hist(sample_distribution_median, bins=20, alpha=0.4, label=\"median\", density=True)\n",
    "plt.hist(sample_distribution_inter_quartile_range, bins=20, alpha=0.4, label=\"inter quartile range\", density=True)\n",
    "plt.axvline(x=true_median, c=\"black\", label=\"true median\")\n",
    "plt.axvline(x=true_IQR, c=\"gray\", label=\"true IQR\")\n",
    "plt.ylabel(\"rel. frequency\")\n",
    "plt.legend();  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all_sample_means = []\n",
    "sample_sizes = (1,2,4,16, 32)\n",
    "for j in sample_sizes: # TODO: without loops\n",
    "    samples = []\n",
    "    for i in range(20000): # without loop?\n",
    "        samples.append(np.random.choice(population, size=(j), replace=True))\n",
    "    means = np.array(samples).mean(axis=1)\n",
    "    all_sample_means.append(means)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10,8))\n",
    "plt.title(\"approximated means\")\n",
    "for i, j in enumerate(sample_sizes):\n",
    "    plt.hist(all_sample_means[i], bins=20, alpha=0.4, label=\"sample size:\"+str(j), density=True)\n",
    "plt.axvline(x=population.mean(), c=\"black\", label=\"population mean\")\n",
    "plt.ylabel(\"rel. frequency\")\n",
    "plt.legend();  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "var_mean_estimator_16 = np.var(all_sample_means[3], ddof=1)\n",
    "var_mean_estimator_32 = np.var(all_sample_means[4], ddof=1)\n",
    "print (\"Variance of estimator with sample size 16:\", var_mean_estimator_16)\n",
    "print (\"Variance of estimator with sample size 32:\", var_mean_estimator_32)\n",
    "print (\"quotient\" , var_mean_estimator_32 / var_mean_estimator_16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Note that the variance of the estimator nearly halfs if the sample size is doubled.\n",
    "- Note if you change the sampling procedure from replace=True to replace=False this statement holds not any more. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As preparation solve the multiple choise questions: [biased-unbiased-estimators](https://www.khanacademy.org/math/ap-statistics/sampling-distribution-ap/what-is-sampling-distribution/e/biased-unbiased-estimators)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "\n",
    "Consider independent random variables $X_1$, $X_2$, and $X_3$ with\n",
    "- $\\mathbb E[X_1] = 1$, $std[X_1] = 4$\n",
    "- $\\mathbb E[X_2] = 2$, $var[X_2] = 9$\n",
    "- $\\mathbb E[X_3] = 3$, $std[X_3] = 5$\n",
    "\n",
    "$var$ is the variance and $std$ the standard deviation of the random variables.\n",
    "\n",
    "Note the variance of a random variable is $var(X) = \\mathbb E[(X - \\mathbb E(X))^2]$.\n",
    "\n",
    "Show from this definition that \n",
    "$var(\\alpha X) = \\alpha^2 var[X]$\n",
    "\n",
    "Calculate\n",
    "1. $\\mathbb E[4X_1 + 2X_2 − 6X_3]$ \n",
    "2. $Var[4X_1 + 2X_2 − 6X_3]$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "\n",
    "Let $X_1, X_2, \\dots , X_n$ denote a i.i.d. random sample from a population with true mean $\\mu=\\mathbb E[X]$ and true variance $\\sigma^2_X= var(X)=\\mathbb E \\left[X^2 \\right] - \\mu^2 $.\n",
    "\n",
    "Consider the following estimators of $\\mu$:\n",
    "\n",
    "1. $\\hat \\mu_1 = \\frac{1}{n}\\left(2 X_1 + 2 X_2 + \\sum_{i=3}^{n} X_i\\right)$\n",
    "2. $\\hat \\mu_2 = \\frac{1}{2 + n}\\left(2 X_1 + 2 X_2 + \\sum_{i=3}^{n} X_i\\right)$\n",
    "3. Draw two different $X_i$ and $X_j$ with $i\\neq j$ from the sample and calulate $\\hat \\mu_3 = \\frac{X_i + X_j}{2}$: \n",
    "\n",
    "Calulate the bias of all three estimators. Are the estimators unbiased?\n",
    "\n",
    "Calculate the variance for $\\hat \\mu_3$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "\n",
    "Sample 10000 data sets from a normal distribution $\\mathcal N(\\mu=3, \\sigma^2=4)$ with 8 data points each.\n",
    "\n",
    "Plot a normalized histogram of the data.\n",
    "\n",
    "Plot in the same diagram a histogram of the estimator values of $\\mu_1$.\n",
    "This histogram visualizes the _sampling distribution_.\n",
    "\n",
    "Can you read out the bias and the variance from the plot? \n",
    "\n",
    "Estimate the bias and variance of the estimator $\\mu_1$ from the data."
   ]
  },
  {
   "attachments": {
    "image.png": {
     "image/png": "iVBORw0KGgoAAAANSUhEUgAAAmsAAAHVCAYAAACuZLx8AAAgAElEQVR4Ae3dC/AlVX0n8O8IIhpZo+MYyAAyTEBBeWhGcNVFNlEhLzCKKzGpEkLJorLRtbSCJUELI0atGNddEsQAkhSPiNkVTDCsiWJMiIQZmAFBHgPIq5LNgIpjBAFh6/ynr/bc/6vvzO37v/f251R13X6cPn3O5/TM/Ob0K5EIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAsumhWD58uVP7LXXXtPSHO0gQGBCBW655ZaZmj/vec8bbguqcjPscodbS6URINBQYN26dfcnWdEk+45NMk1CnhKorV27dhKqqo4ECEyxwOGHHz7TuiuvvHK4razKzbDLHW4tlUaAQEOBZcuW3dUwa57UNKN8BAgQIECAAAECoxcQrI3e3BEJECBAgAABAo0FBGuNqWQkQIAAAQIECIxeYGruWRs9nSMSIECAAAECgwg8+uijuffee/Pwww8PsttE5915552z++6758lPfvI2t0Owts10diRAgAABAgQGESiB2i677JLyUOCyZVPzQop5CZ544ok88MADMwHqqlWr5s232AaXQRcTsp0AAQIECBAYikAZUVu+fHknArUCVgLS0t7tHUkUrA3l9FMIAQIECBAg0ESgCyNqdYdhtFewVhc1T4AAAQIECBAYMwHB2ph1iOoQIECAAAECBOoCgrW6hnkCBAgQIECAwJgJeBp0zDpEdQgQIECAQBcE3vnOd2b9+vVDberBBx+cT3ziE4uWWT4Ld9ZZZ+X5z3/+zNOar3zlK/ONb3xj0f2WKoORtaWSd1wCBAgQIEBgSQQ2btyYfffdd+bY119/fQ444IAlqUfTgxpZayolHwECBAgQIDA0gSYjYEM7WK2gu+66KytXrsyTnrRlvKoEawceeGAtx/jNGlkbvz5RIwIECBAgQKAlgQ0bNmwVnK1bt26r5ZYOu13FCta2i8/OBAgQIECAwCQJlPvkei+pve2223LppZeO/WVQwdoknWHqSoAAAQIECGyXQBlZe/zxx3PQQQfl9NNPz/7775/zzz9/u8pse2f3rLUtrHwCBAgQIEBgbATKPWrXXnvtzDdKx6ZSi1TEyNoiQDYTIECAAAEC0yGwefPmme91lo/JT1ISrE1Sb6krAQIECBAgsM0CJUi79dZbt3n/pdrRZdClkndcAgQI9AusPa9/zU+WN//rlvlenjXH/2SbOQIEplrAyNpUd6/GESBAgAABApMuIFib9B5UfwIECBAgQGCqBQRrU929GkeAAAECBAhMuoBgbdJ7UP0JECBAgACBqRYQrE1192ocAQIECBAgMOkCgrVJ70H1J0CAAAECBKZaQLA21d2rcQQIECBAgMD2CNxwww3ZddddU36XKgnWlkrecQkQIECAAIGxFzjjjDNy1VVXpfwuVRKsLZW84xIgQIAAAQJLIvCpT30qu+22Ww4++OAfT2Xk7KlPferMcr1SF110Ufbee++U3/nSQw89NLPfTjvtlPvvv3++bNu83hcMtpnOjgQIECBAgMAkCpTA7Pd///dzwgkn/Lj63/rWt7J69eqsX7/+x+uazpQgr+y31157Nd1loHxG1gbikpkAAQIECBCYdIHrr79+1gjaXG06/PDDc/PNN89seuCBB/LCF75wrmytrzOy1jqxAxAgQIAAAQLjJHDjjTfm+OOPz5OetGXM6m1ve1te85rXzKrixo0bs++++86sLwHeAQccMCvPKFYI1kah7BgECBAgQIDA1gLvfGeyDZccty6kb+ngg5NPfKJv5daL99xzT1asWJESfNVTuQxaT3fddVdWrlz544Cu5D/wwAPrWUY27zLoyKgdiAABAgQIEFhqgXK/2n777bdoNTZs2LBVcLZu3bqZ5TvuuGPmXrdjjjlm0TKGlcHI2rAklUOAAAECBAg0F1hkBKx5QYPlLCNkz3/+8xfdqTww8PDDD8/ku+2223LppZfOPJSw55575pxzzolgbVFCGQgQIECAAAECgwuUkbWvfvWr+eIXvziz87Jly/K1r31tVkFlZG3nnXfOQQcdNDOitv/+++f888/P7/3e783K2/YKI2ttCyufAAECBAgQGBuBCy64YM669L8frYzAXXvttdlll13mzD/Kle5ZG6W2YxEgQIAAAQJjKbDDDjvkwQcfnHmlx+bNm1NG3OYK1MorPE466aRcd911+fCHPzzTlt5LcR999NEfP5AwzEYaWRumprIIECBAgACBiRTYY489Up4U7aVbb721N7vV7/Lly3PWWWdtta73UtytVg5xwcjaEDEVRYAAAQIECBAYtkDbwdqRSW5JsjHJKQtU/vVJnkiyppbnvdV+Zf8jauvNEiBAgAABAgQ6I9DmZdAdkpyZ5NVJ7k1yTZLLktzUp1vu3HtHkqtr6/dPcmySFyT52SR/m6S8QvhHtTxmCRAgQIAAAQJTL9BmsHZINTJ2R6V4cZKj5wjWPpjkI0neU9Mu+Ur+Hya5syqnlPdPtTxmCRAg0F2Btec1b/ua45vnlZMAgbETaPMy6MokP7lTb8voWllXTy9OskeSv66vTNJk37LLiUnWlmnTpk19RVgkQIAAAQIECEy+QJsja4vplEDx40mOWyzjAtvPTlKm8p2vcs+bRIAAAQIECBCYKoE2g7X7qlGzHtjuScq6Xir3qr0wyZXVil2re9qOqvKVEbde6t+3t94vAQIECBAgQGCqBdoM1soDBfskWVUFX+WBgTfVNB9M8uzacgna3l1d1nwoyYXVyFt5wKCU88+1vGYJECBAgACBCRe48Oq7h9qCNx2650DlfeADH8jTn/70vPvdJfyYnT7/+c9n3333TfnU1FKmNu9ZeyzJyUmuSPLNJJ9NcmOS05OU0bOFUslX8pcnR/8myds9CboQl20ECBAgQIDAsAVKsHbTTf0vsRj2URYvr81grRz98uqVG6uTfKiqzmnV5c7+2h1ejar11pf8Zb/nJdnytdXeFr8ECBAgQIAAgW0Q+NCHPjQzWvaKV7wit9xSXuWafPrTn85LXvKSmY+2v/71r88PfvCDXHXVVbnsssvynve8Z+YTVLfffvuc+bahCgPv0nawNnCF7ECAAAECBAgQaENg3bp1ufjii7N+/fpcfvnlueaacsdW8rrXvW5mfsOGDdlvv/1yzjnn5GUve1mOOuqofOxjH5vJv3r16jnztVHP/jLbvGet/1iWCRAgQIAAAQJLJvC1r30tv/7rv56nPe1pM3UowVhJ3/jGN3Lqqafmu9/9br7//e/niCPm/nBS03zDbqBgbdiiyiNAgAABAgQmSuC4445LuT/toIMOymc+85lceWXvRRVbN6Npvq332v4ll0G331AJBAgQIECAwAQIHHbYYTNB2UMPPZTNmzfnC1/4wkyty/xuu+2WRx99NBdccMGPW7LLLrvM5OutmC9fb3tbv0bW2pJVLgECBAgQILCgwKCv2liwsAYbX/ziF+eNb3zjzAjac57znJmHCspuH/zgB3PooYeWF+zP/JagrKRjjz02b3nLW/LJT34yn/vc5+bN1+DQ25VFsLZdfHYmQIAAAQIEJkngfe97X8rUn9761rf2r8rLX/7yrV7dUfLMlW/WjkNeIVgbMqjiCBAgsJXAIB9c32pHCwQIENgi4J41ZwIBAgQIECBAYIwFBGtj3DmqRoAAAQIECBAQrDkHCBAgQIAAAQJjLCBYG+POUTUCBAgQIECAgGDNOUCAAAECBAgQGGMBwdoYd46qESBAgAABAgQEa84BAgQIECBAoHMCN9xwQ3bdddeU33FPgrVx7yH1I0CAAAECBIYucMYZZ+Sqq65K+R335KW4495D6keAAAECBAgMXeCiiy6aKbP3O/QDDLFAwdoQMRVFgAABAgQIDCAw7C98rDl+gINPTlaXQSenr9SUAAECBAgQGILA4YcfnptvvnmmpAceeCAvfOELh1Bqe0UI1tqzVTIBAgQIECAwhgIbN27MvvvuO1Oz66+/PgcccMAY1vInVRKs/cTCHAECBAgQIDDlAnfddVdWrlyZJz1pSwhUgrUDDzxwrFstWBvr7lE5AgQIECBAYJgCGzZs2Co4W7du3czyHXfckRNOOCHHHHPMMA83lLIEa0NhVAgBAgQIECAwCQLr16/Pww8/PFPV2267LZdeeunMZdC9994755xzzlg2QbA2lt2iUgQIECBAgEAbAmVk7fHHH89BBx2U008/Pfvvv3/OP//8Ng41tDK9umNolAoiQIAAAQIEBhJYgldtlHvUrr322uyyyy4DVXUpMxtZW0p9xyZAgAABAgRGJrB58+YsW7ZszkCtvMLjpJNOynXXXZcPf/jDI6tTkwMZWWuiJA8BAgQIECAw8QJlNO3WW2+dsx3Lly/PWWedNee2pV5pZG2pe8DxCRAgQIAAAQILCAjWFsCxiQABAgQIECCw1AKCtaXuAccnQIAAAQIECCwgIFhbAMcmAgQIECBAYLgCTzzxxHALHPPShtFewdqYd7LqESBAgACBaRHYeeedU566HEYAMwkmpZ2lvaXd25M8Dbo9evYlQIAAAQIEGgvsvvvuuffee7Np06bG+0x6xhKolXZvTxKsbY+efQkQIECAAIHGAk9+8pOzatWqxvll3CLgMqgzgQABAgQIECAwxgKCtTHuHFUjQIAAAQIECAjWnAMECBAgQIAAgTEWEKyNceeoGgECBAgQIEBAsOYcIECAAAECBAiMsYBgbYw7R9UIECBAgAABAoI15wABAgQIECBAYIwF2g7WjkxyS5KNSU6Zw+GkJDckWZ/kH5LsX+XZK8lD1fqy7aw59rWKAAECBAgQIDD1Am2+FHeHJGcmeXWSe5Nck+SyJDfVVC+sBWJHJfl4khLglXR7koOreT8ECBAgQIAAgU4KtDmydkg1onZHkkeSXJzk6D7l79WWfypJt77uWmu8WQIECBAgQIDAXAJtBmsrk9xTO2gZXSvr+tPbq1G0jyb5ndrG8j2K65J8Ncl/qq2vz56YZG2ZuvSdsTqAeQIECBAgQGC6BdoM1prKlUulq5P8bpJTq53+JcmeSV6U5F1JyuXS/zBHgWcnWVOmFStWzLHZKgIECBAgQIDAZAu0Gazdl2SPGk/55HxZN18ql0lfW238YZIHqvl11cjbvvPtaD0BAgQIECBAYFoF2gzWygMF+yQplzN3SnJs9YBB3bJs76VfSXJbtVCGycoDCiXtXZVT7n2TCBAgQIAAAQKdEmjzadDHkpyc5Ioq8Do3yY1JTq/uMytPhpbtr0ryaJLvJHlzpX9Yla+sfzxJecXHtzvVMxpLgAABAgQIEEjSZrBWgC+vpjr2abWFd9Tm67N/maRMEgECBAgQIECg0wJtXgbtNKzGEyBAgAABAgSGISBYG4aiMggQIECAAAECLQkI1lqCVSwBAgQIECBAYBgCgrVhKCqDAAECBAgQINCSgGCtJVjFEiBAgAABAgSGISBYG4aiMggQIECAAAECLQkI1lqCVSwBAgQIECBAYBgCgrVhKCqDAAECBAgQINCSQNsvxW2p2oolQIAAgcYCa89rlnXN8c3yyUWAwEgFjKyNlNvBCBAgQIAAAQKDCQjWBvOSmwABAgQIECAwUgHB2ki5HYwAAQIECBAgMJiAYG0wL7kJECBAgAABAiMVEKyNlNvBCBAgQIAAAQKDCQjWBvOSmwABAgQIECAwUgHB2ki5HYwAAQIECBAgMJiAYG0wL7kJECBAgAABAiMVEKyNlNvBCBAgQIAAAQKDCQjWBvOSmwABAgQIECAwUgHB2ki5HYwAAQIECBAgMJiAYG0wL7kJECBAgAABAiMVEKyNlNvBCBAgQIAAAQKDCQjWBvOSmwABAgQIECAwUgHB2ki5HYwAAQIECBAgMJiAYG0wL7kJECBAgAABAiMVEKyNlNvBCBAgQIAAAQKDCQjWBvOSmwABAgQIECAwUgHB2ki5HYwAAQIECBAgMJiAYG0wL7kJECBAgAABAiMVEKyNlNvBCBAgQIAAAQKDCQjWBvOSmwABAgQIECAwUgHB2ki5HYwAAQIECBAgMJiAYG0wL7kJECBAgAABAiMVEKyNlNvBCBAgQIAAAQKDCQjWBvOSmwABAgQIECAwUgHB2ki5HYwAAQIECBAgMJiAYG0wL7kJECBAgAABAiMV2HGkR3MwAgQITIvA2vPmbsnmf92yfr7tc+9lLQECBOYVMLI2L40NBAgQIECAAIGlF2g7WDsyyS1JNiY5ZY7mnpTkhiTrk/xDkv1red5b7Vf2P6K23iwBAgQIECBAoDMCbQZrOyQ5M8kvVUHYb/QFYwX5wiQHJDk4yUeTfLySL0HbsUlekKQEfH+cpJQnESBAgAABAgQ6JdBmsHZINTJ2R5JHklyc5Og+3e/Vln8qyRPVcslX8v8wyZ1VOaU8iQABAgQIECDQKYE2HzBYmeSemua9SQ6tLfdm357kXUl2SvIL1cqy79d7GZKUfcs6iQABAgQIECDQKYE2R9aaQpZLpauT/G6SU5vuVOU7McnaMm3atGnAXWUnQIAAAQIECIy/QJvB2n1J9qgR7J6krJsvlcuer602Nt337CRryrRixYr5yrWeAAECBAgQIDCxAm0Ga9ck2SfJquoSZ3lg4LI+qbK9l34lyW3VQslX8j+l2r/k++deRr8ECBAgQIAAga4ItHnP2mNJTk5yRfUk57lJbkxyenXpsgRkZfurkjya5DtJ3lzBl3yfTXJTklJOua/tR13pFO0kQIAAAQIECPQE2gzWyjEur6be8crvabWFd9Tm+2c/lKRMEgECBAgQIECgswJtXgbtLKqGEyBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCgGCtBVRFEiBAgAABAgSGJSBYG5akcggQIECAAAECLQgI1lpAVSQBAgQIECBAYFgCgrVhSSqHAAECBAgQINCCQJNg7Q+TvKCFYyuSAAECBAgQIEBgEYEmwdo3k5yd5OokJyV5xiJl2kyAAAECBAgQIDAkgR0blPOnScr0vCTHJ7k+yT8m+XSSrzTYXxYCBAgQmASBtec1r+Wa8s+BRIDAKASajKyVeuyQ5PnVdH+SDUneleTiUVTSMQgQIECAAAECXRVoEqz9UZKbk/xykjOS/HySjyT5tSQvWgTuyCS3JNmY5JQ58paA76ZqtO7vkjy3ludHSdZX02W19WYJECBAgAABAp0RaHIZtFz2PDXJv8+hcsgc63qrymjcmUleneTeJNckKUFXCc566boka5L8IMlbk3w0yRurjQ8lObiX0S8BAgQIECBAoIsCTUbWvpukHtT9dJLXVlgPLoBWArkyonZHkkeqS6ZH9+Uv97yVQK2kryfZvZr3Q4AAAQIECBAgkKRJsPb+JPWgrARvZd1iaWWSe2qZyuhaWTdfOiHJF2sbd06ytgriesFhbfPM7IlVnrWbNm3q32aZAAECBAgQIDDxAvURs/kaM1dA12S/+cqba/1vVZdDX1nbWO5fuy/J3km+nOSGJLfXtpfZ8kqRMmXFihVP9G2zSIAAAQIECBCYeIG5ArH+RpXRrY8nWV1NZX5df6Y5lkugtUdtfbnEWdb1p1cleV+So5L8sLaxl7dcRr2ywcMMtV3NEiBAgAABAgSmQ6BJsPbfqnvO/iJJmUpA9fYGzS8PFOyTZFWSnZIcWz1gUN+1PE36qSpQ+7fahmcmeUq1/OwkL+97MKGW1SwBAgQIECBAYHoFmlzOLE+BzvXajcVUHktycpIrqve0nZvkxiSnV/eZlSdDP5bk6UkuqQq7uwrc9quCuMer++r+QLC2GLftBAgQIECAwDQKNAnW9k3y7iR79T0V+gsNQC5PUqZ6Oq22UC6BzpWuSnLAXBusI0CAAAECBAh0SaBJsFZGvc6qPjlVXlQrESBAgAABAgQIjEigSbBWLmf+yYjq4zAECBAgQIAAAQI1gSYPGHwhyduS7JbkWbWpVoxZAgQIECBAgACBNgSajKy9uTrwe2oVKO80K+8/kwgQIECAAAECBFoUaBKslVdvSAQIECBAgAABAksg0OQy6NOqD7nPfCmgenfary5BXR2SAAECBAgQINA5gSbB2nnVS3FfVumULwv8fuekNJgAAQIECBAgsAQCTYK18pmpjyZ5tKrfD5IsW4K6OiQBAgQIECBAoHMCTYK1R5I8NUnvQ+kleKt/w7NzaBpMgAABAgQIEBiVQJMHDN6f5G+qj7JfUH2n87hRVdBxCBAgQIAAAQJdFmgSrH0pybVJXlpd/nxHkvu7jKbtBAgQIECAAIFRCTQJ1g6rKrO5+t2/+v37UVXScQgQIECAAAECXRVoEqzVX4a7c5JDkqxL0uRD7l111W4CBAgQIECAwFAEmgRrv9Z3pD2SfKJvnUUCBAgQIECAAIEWBJo8Ddp/2HuT7Ne/0jIBAgQIECBAgMDwBZqMrP3P2ms7SnB3cPXAwfBro0QCBAgQIECAAIGtBJoEa2trezyW5KIk/1hbZ5YAAQIECBAgQKAlgSbB2vktHVuxBAgQIECAAAECiwg0CdZuqF0GrRdXPjlVvmpwYH2leQIECBAgQIAAgeEJNAnWvlgd7s+r39+sfv9keNVQEgECBAgQIECAwFwCTYK1Vyd5UW3nU6oHDMqvRIAAAQIECBAg0KJAk1d3lMudL6/V4WVJmuxX28UsAQIECBAgQIDAtgg0GVk7Icm5SZ5RHeC7SX57Ww5mHwIECBAgQIAAgcEEmgRr5dNSB9WCtQcHO4TcBAgQIECAAAEC2yrQ5HLmzyQ5J8nFSUqgVj7kXkbbJAIECBAgQIAAgZYFmgRrn0lyRZKfrepya5J3tlwvxRMgQIAAAQIECDR8UODZST6b5PFKrHzF4Ef0CBAgQIAAAQIE2hdoMrL270mW116M+9Lqcmj7tXMEAgQIECBAgEDHBZo8YPCuJJclWV19E3RFkmM67qb5BAgQIECAAIGRCCwWrJWRt52TvDLJ85KUd67dkuTRkdTOQQgQIECAAAECHRdYLFgr96mdWX3B4MaOW2k+AQIECBAgQGDkAk3uWfu7JK+vRtVGXkEHJECAAAECBAh0WaBJsPZfk1yS5IdJvpdkc/XbZTdtJ0CAAAECBAiMRGChYK33PdDyQEHJt1OS/5Bkl+p3JBV0EAIECBAgQIBAlwUWCtY+WcFc1WUgbSdAgAABAgQILKXAQg8YlCc+z06ye5Je4Fav6+/UF8wTIECAAAECBAgMX2ChYO1Xk7wqyRFJysfcJQIECBAgQIAAgRELLBSs3V99vP2bSTaMuF4OR4AAAQIECBAg0PDboAI1pwoBAgQIECBAYIkEFnrAYImq5LAECBAgQIAAAQI9gbaDtSOrz1NtTHJK76C13/Ld0ZuSXJ+kvHz3ubVtb05yWzWVeYkAAQIECBAg0DmBbQ3WXtxAaofqU1W/lGT/JL9R/dZ3vS7JmiQHJvlcko9WG5+V5P1JDk1ySDX/zPqO5gkQIECAAAECXRDY1mDtrQ1wSpBVRtTuSPJI9bDC0X37fSXJD6p1X69eE1IWyxOoX0ry7STfqebLKJ1EgAABAgQIEOiUwLYGa29poLQyyT21fPcmKevmSyck+WK1sem+JyZZW6ZNmzbNV671BAgQIECAAIGJFVjo1R2LXeq8doit/q3qcugrByyzvLS3TFmxYsUTA+4rOwECBBYVuPDqu+fMs/ruMvA/O33v4fI+8eTqO+fePnuPLWsOXVXu/pAIECAwW2ChYO0PZ2f/8ZoSGP3Cj5fmnrkvyR61TeVLCGVdfyov3n1fkhKolY/Fl1TyHV7Nl5+y75W1ZbMECBAgQIAAgU4ILBSs/eftFLgmyT5JVlXB17FJ3tRX5ouSfCpJuR/t32rbrkhyRpLeQwWvSfLe2nazBAgQIECAAIFOCDS5Z+1pSU7tXW6sArDyKarF0mNJTk5SAq/yFYTPJrkxyelJjqp2/liSpye5JMn6JJdV68v1gw8mKQFfmco+g11TWKx2thMgQIAAAQIEJkBgoZG1XvXPq74N+rJqRblEWYKrv+plWOD38iRlqqfTagvlEuh86dwkZZIIECAwGoG15a+7rdN896ZtncsSAQIE2hNoMrK2unr/2Za7Zre8amNZe1VSMgECBAgQIECAQE+gSbBW3pH21CS9py1L8NZ7EKBXjl8CBAgQIECAAIEWBJpcBi1fEvib6snOC5K8PMlxLdRFkQQIECBAgAABAn0CiwVr5XLnzUlel+SlScryO5Lc31eORQIECAHMUAIAABalSURBVBAgQIAAgRYEFgvWyqXP8oDAAUn+uoXjK5IAAQIECBAgQGABgSb3rJUvFbxkgTJsIkCAAAECBAgQaElgsZG1cthDk/xmkruS/Ht1KbSMuB3YUp0US4AAAQIECBAgUAk0CdaOoEWAAAECBAgQILA0Ak2CtTKiJhEgQIAAAQIECCyBQJN71pagWg5JgAABAgQIECBQBARrzgMCBAgQIECAwBgLNLkMOsbVVzUCBLoscOHVdw+1+b4DOlROhREgMCQBwdqQIBVDgACB7RG4+s5vL7j7fg9v+TzzNxfJt1Ahh6561kKbbSNAYEwFXAYd045RLQIECBAgQIBAERCsOQ8IECBAgAABAmMsIFgb485RNQIECBAgQICAYM05QIAAAQIECBAYYwHB2hh3jqoRIECAAAECBARrzgECBAgQIECAwBgLCNbGuHNUjQABAgQIECAgWHMOECBAgAABAgTGWECwNsado2oECBAgQIAAAcGac4AAAQIECBAgMMYCgrUx7hxVI0CAAAECBAgI1pwDBAgQIECAAIExFhCsjXHnqBoBAgQIECBAQLDmHCBAgAABAgQIjLGAYG2MO0fVCBAgQIAAAQKCNecAAQIECBAgQGCMBQRrY9w5qkaAAAECBAgQEKw5BwgQIECAAAECYywgWBvjzlE1AgQIECBAgIBgzTlAgAABAgQIEBhjAcHaGHeOqhEgQIAAAQIEBGvOAQIECBAgQIDAGAsI1sa4c1SNAAECBAgQICBYcw4QIECAAAECBMZYQLA2xp2jagQIECBAgAABwZpzgAABAgQIECAwxgJtB2tHJrklycYkp8zhcFiSa5M8luSYvu0/SrK+mi7r22aRAAECBAgQINAJgR1bbOUOSc5M8uok9ya5JkkJum6qHfPuJMcleXdtXW/2oSQH9xb8EiBAgAABAgS6KNBmsHZINaJ2RwV7cZKj+4K1b1XbHu8ivjYTIECAAAECBBYTaPMy6Mok99QqUEbXyrqmaecka5N8Pclrm+4kHwECBAgQIEBgmgTaHFnbXqfnJrkvyd5JvpzkhiS39xV6YpIyZdOmTX2bLBIgQIAAAQIEJl+gzZG1EmjtUSPavQq+aqsWnC37l1Quo16Z5EXVcv3n7CRryrRixYr6evMECBAgQIAAgakQaDNYKw8U7JNkVZKdkhxbPWDQBO6ZSZ5SZXx2kpf33evWpAx5CBAgQIAAAQITL9BmsFZex3FykiuSfDPJZ5PcmOT0JEdVci+pnhR9Q5JPVdvLpv2q+9U2JPlKkj8QrE38uaYBBAgQIECAwDYItH3P2uVJylRPp9UWyuhbuTzan65KckD/SssECBAgMCYCa89rXpE1xzfPKycBArME2hxZm3UwKwgQIECAAAECBAYTEKwN5iU3AQIECBAgQGCkAm1fBh1pYxyMAIHxEbjw6vKBEokAAQIEtlfAyNr2CtqfAAECBAgQINCigGCtRVxFEyBAgAABAgS2V0Cwtr2C9idAgAABAgQItCjgnrUWcRVNgACBcRK4+s5vj6Q6h6561kiO4yAEuiJgZK0rPa2dBAgQIECAwEQKCNYmsttUmgABAgQIEOiKgGCtKz2tnQQIECBAgMBECgjWJrLbVJoAAQIECBDoioBgrSs9rZ0ECBAgQIDARAoI1iay21SaAAECBAgQ6IqAYK0rPa2dBAgQIECAwEQKCNYmsttUmgABAgQIEOiKgGCtKz2tnQQIECBAgMBECgjWJrLbVJoAAQIECBDoioBgrSs9rZ0ECBAgQIDARAoI1iay21SaAAECBAgQ6IqAYK0rPa2dBAgQIECAwEQKCNYmsttUmgABAgQIEOiKgGCtKz2tnQQIECBAgMBECgjWJrLbVJoAAQIECBDoioBgrSs9rZ0ECBAgQIDARAoI1iay21SaAAECBAgQ6IqAYK0rPa2dBAgQIECAwEQKCNYmsttUmgABAgQIEOiKwI5daah2EiDQTYHVd1/SzYZrNQECUyNgZG1qulJDCBAgQIAAgWkUEKxNY69qEwECBAgQIDA1AoK1qelKDSFAgAABAgSmUUCwNo29qk0ECBAgQIDA1AgI1qamKzWEAAECBAgQmEYBwdo09qo2ESBAgAABAlMjIFibmq7UEAIECBAgQGAaBQRr09ir2kSAAAECBAhMjYBgbWq6UkMIECBAgACBaRQQrE1jr2oTAQIECBAgMDUCgrWp6UoNIUCAAAECBKZRQLA2jb2qTQQIECBAgMDUCLQdrB2Z5JYkG5OcMofaYUmuTfJYkmP6tr85yW3VVOYlAgQIECBAgEDnBHZsscU7JDkzyauT3JvkmiSXJbmpdsy7kxyX5N21dWX2WUnen2RNkieSrKv2/U5fPosECBAgQIAAgakWaHNk7ZBqRO2OJI8kuTjJ0X2a30pyfZLH+9YfkeRLSb6dpARoZb6M0kkECBAgQIAAgU4JtBmsrUxyT02zjK6VdU1S031PTLK2TJs2bWpSrjwECBAgQIAAgYkSaPMy6Cggzk5SpqxYsaJcLpUIECBAgAABAlMl0ObI2n1J9qhp7Z6krGuStmffJuXLQ4AAAQIECBCYCIE2g7XyQME+SVYl2SnJsdVDAk1grkjymiTPrKYyX9ZJBAgQIECAAIFOCbQZrJXXcZxcBVnfTPLZJDcmOT3JUZXyS6onRd+Q5FPV9rKpPFjwweoJ0hL0lX3KOokAAQIECBAg0CmBtu9ZuzxJmerptNpCCcTK5dG50rlJyiQRIECAAAECBDor0ObIWmdRNZwAAQIECBAgMCwBwdqwJJVDgAABAgQIEGhBQLDWAqoiCRAgQIAAAQLDEmj7nrVh1VM5BAgMSeDCq8tX3iQCBAgQmBQBwdqk9JR6EiBAYFIF1p7XrOZrjm+WTy4CHRMQrHWswzWXAAECbQtcfee2vWnp9h81H/V906F7tt0M5RMYGwH3rI1NV6gIAQIECBAgQGC2gGBttok1BAgQIECAAIGxERCsjU1XqAgBAgQIECBAYLaAYG22iTUECBAgQIAAgbEREKyNTVeoCAECBAgQIEBgtoBgbbaJNQQIECBAgACBsREQrI1NV6gIAQIECBAgQGC2gGBttok1BAgQIECAAIGxERCsjU1XqAgBAgQIECBAYLaAYG22iTUECBAgQIAAgbEREKyNTVeoCAECBAgQIEBgtoBgbbaJNQQIECBAgACBsREQrI1NV6gIAQIECBAgQGC2gGBttok1BAgQIECAAIGxERCsjU1XqAgBAgQIECBAYLaAYG22iTUECBAgQIAAgbEREKyNTVeoCAECBAgQIEBgtoBgbbaJNQQIECBAgACBsREQrI1NV6gIAQIECBAgQGC2gGBttok1BAgQIECAAIGxERCsjU1XqAgBAgQIECBAYLaAYG22iTUECBAgQIAAgbEREKyNTVeoCAECBAgQIEBgtoBgbbaJNQQIECBAgACBsREQrI1NV6gIAQIECBAgQGC2gGBttok1BAgQIECAAIGxERCsjU1XqAgBAgQIECBAYLaAYG22iTUECBAgQIAAgbEREKyNTVeoCAECBAgQIEBgtsCOs1dZQ4AAgfEXWH33JeNfSTUkQIDAEASMrA0BUREECBAgQIAAgbYEBGttySqXAAECBAgQIDAEAcHaEBAVQYAAAQIECBBoS0Cw1pascgkQIECAAAECQxBoO1g7MsktSTYmOWWO+j4lyV9U269OsleVp/w+lGR9NZ01x75WESBAgAABAgSmXqDNp0F3SHJmklcnuTfJNUkuS3JTTfWEJN9J8nNJjk3ykSRvrLbfnuTgWl6zBAgQIECAAIHOCbQ5snZINWJ2R5JHklyc5Og+4bJ8frXuc0l+McmyvjwWCRAgQIAAAQKdFWhzZG1lkntqsmV07dDacpmt53ksyYNJlld5ViW5Lsn3kpya5Gt9+5bFE6spmzZtmmOzVQQmS+DCq++erAqrLYElEhjFn5U3HbrnErXOYQlsLdBmsLb1kQZb+pck5U/JA0l+Psnnk7ygCtzqJZ2dpExZsWLFE/UN5gkQIECAAAEC0yDQ5mXQ+5LsUUPaPUlZV0/1PCVwfEYVoP2w+i151yUp96/tW9/RPAECBAgQIECgCwJtBmvlgYJ9kpTLmTtVDxCUBwzqqSy/uVpxTJIvJykjZCuSlAcUStq7Kqfc+yYRIECAAAECBDol0OZl0HIP2slJrqgCr3OT3Jjk9CRrqydDz0ny59WDCN+uArrSAYdV+R5N8niSk5KU7RIBAgQIECBAoFMCbQZrBfLyaqqjnlZbeDjJG2rLvdm/TFImiQABAgQIECDQaYE2L4N2GlbjCRAgQIAAAQLDEBCsDUNRGQQIECBAgACBlgQEay3BKpYAAQIECBAgMAwBwdowFJVBgAABAgQIEGhJQLDWEqxiCRAgQIAAAQLDEGj7adBh1FEZBAgQINABgdV3X9K4lbfvOdeLBBrvLiOBiRIwsjZR3aWyBAgQIECAQNcEBGtd63HtJUCAAAECBCZKQLA2Ud2lsgQIECBAgEDXBARrXetx7SVAgAABAgQmSkCwNlHdpbIECBAgQIBA1wQEa13rce0lQIAAAQIEJkpAsDZR3aWyBAgQIECAQNcEBGtd63HtJUCAAAECBCZKQLA2Ud2lsgQIECBAgEDXBARrXetx7SVAgAABAgQmSkCwNlHdpbIECBAgQIBA1wQEa13rce0lQIAAAQIEJkpAsDZR3aWyBAgQIECAQNcEduxag7WXAIHxFVh99yXjWzk1I0CAwBIJCNaWCN5hJ0/gwqvvnrxKqzEBAgQITLyAy6AT34UaQIAAAQIECEyzgJG1ae5dbSNAgACBbRYY1Wj6mw7dc5vraMduCAjWutHPWkmAAIGpEhjk/sbb93zDVLVdY7on4DJo9/pciwkQIECAAIEJEhCsTVBnqSoBAgQIECDQPQHBWvf6XIsJECBAgACBCRIQrE1QZ6kqAQIECBAg0D0BwVr3+lyLCRAgQIAAgQkSEKxNUGepKgECBAgQINA9AcFa9/pciwkQIECAAIEJEhCsTVBnqSoBAgQIECDQPQEvxe1en2sxgZEKDPLy0pFWzMEIECAwIQJG1iako1STAAECBAgQ6KaAkbVu9vtUtXpU3++bKjSNIUBgbARG8XeY74+OTXdvU0WMrG0Tm50IECBAgAABAqMREKyNxtlRCBAgQIAAAQLbJCBY2yY2OxEgQIAAAQIERiPgnrXRODsKAQIECCyRQNMnkm/f8w1LVEOHJbCwgGBtYR9bCRCYQ6DpP35z7GoVgbEVGOS8FtiNbTdOZcXaDtaOTPI/kuyQ5E+T/EGf4lOS/FmSn0/yQJI3JvlWlee9SU5I8qMkv5Pkir59LU6AwCiecpoABlUkQIDAkgqM4u9iT5y218Vt3rNWArQzk/xSkv2T/Eb1W29NCca+k+TnkvxRko9UG0v+Y5O8IEkJ+P64Cvjq+5onQIAAAQIECEy9QJsja4ck2Zjkjkrx4iRHJ7mpplqWP1Atfy7J/0qyrMpX8v8wyZ1VOaW8f6rta3Y7BUbxP63trKLdhyAwyKWdIRxOEQQIdFRgVP+mdHEEr81gbWWSe2rn7L1JDq0tl9l6nseSPJhkebX+67W8Zd+Stz+dmKRMWbdu3feXLVt2S3+GCVx+dpL7J7DeS1FlVoOp8xqh10v/y7sHO1rT3G2V2/T4c+fr4Lm1Xf3bQa+5T5yGa7fy+s2GO01Atuc2rWObwVrTOmxPvrOTlGma0toka6apQS22hdVguLx4DSbQPLdzq7lVycmL10ACbd6zdl+SPWq12T1JWVdP9TwlcHxG9aBBfX3JP9e+9XLMEyBAgAABAgSmUqDNYO2aJPskWZVkp+qBgcv6FMvym6t1xyT5cpInkpT15QGD8rRo2b+U8899+1okQIAAAQIECBDYToFfTnJrktuTvK8q6/QkR1XzOye5pHqAoARje9eOV/KX/cp9aOWJ0q6kmXvwutLY7Wwnq8EAefEaTKB5budWc6uSkxevwQTkJkCAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECwxUoLwUuT7+ur6Zyn99cqXzJody/V140fMpcGTqw7mNJbk5yfZL/k+Sn52lz+WTZDZVneUS+a2mxc6U8uPMX1bl0dZK9ugZUtbc8rf6V6kXdNyZ5xxwOh1fvf+z9+TxtjjxdWrXYn63yUvNPVudW+XP64i7h9LX1ebW/18v5870k7+zL0/Xz69wk/5bkGzWXZyX5UpLbqt9n1rbVZ8vDiSVPmXoPKta3mycwVIESrC32dsXy6a7ysEV5AKM8Wbthjk93DbVSY1rYa5L03g1YPk3W+zxZf3XLPyjlJYpdTE3OlbclOavCKU9dl8Cti2m3WjCxS/UwVPnUXT2Vf0z/qr6i4/OL/dkq/9n8YvUlmpcmKf8ZkLZ8H/tfk/S/BLXr59dh1Z/BerD20dqARBmYmOvv+RLQla8jld8SzJX5+YK6qTj/2nx1x1QAjUkj6p/ueiRJ79NdY1K9kVXj/yYpX7ooqXzhorx/T9paoMm5Uj7zdn61W/nM2y9W/7huXdL0L/1LkmurZm5O8s15vpQy/RLDa2E5t/6segVT+TNaRr9LUNz1VP6Mlf9w39V1iL72/32Sb/etq//9VP6eem3f9rJ4RDXqVvYt3xcvI3HlisLUJsHaeHTtydWlvTIkPNf/Duqf5So1nu/zW+PRmtHU4rer/8HPdbTyrr4S2K3r4CPyTc6Vep76Z97msuzKunIp+EXzjAT9x2o0u4wYvaArIPO0c7E/W/VzqxTh76otkGUE+6J5TJ1fW8P8TJLyH6mSymhkWe5PnTvPepeU+iEsD1fgb5PsOkeR5V1yf5Lkg9X/RMvvHyYpgUhX00JWl1Yoxa0EGRfMg/SK6j7A51T/4yr3uZX/wUkE5hJ4epK/rO4nKvcV1VMZeSuXrr6fpFzi+3z1ku56ni7N+7M1eG+XW1fKu0XfO8euzq85UGqryn8OytT5JFgbzSnwqoaH+fQ898d06fNbi1kdl+RXq0t38/0hLl4llRtXy4MI5dJgV4K1JudKL08Z9ah/5q1i69TPk6tArQT+/3uOlteDt8uT/HF1P+T9c+TtwqrF/mz1zq2ehU8FbnmpewnK/l8Ppfbr/KphVLPFqVw6L6Nr5bf8Pd6fynlW7vfrpXKeXdlb8EugDYH6/Rz/vbofrf845R/UcgNl79Nd5QGDLl6OKfck3JRkRT9QbfmnkpSbxUsq81dN+70MVVt7P03Olbf3PWDw2d7OHfstTy6W+6s+sUC7y4h4yVdSCfrvri1Xqzvz0+TP1q/0PWDgM4Fb7jE+fp6zxPm15Wn0+gMG5an/3hsPym954KA/lQcL7qxuGyq3DpX5sk4i0JrAn1evmSiPuZdvovaCt59NUv4n30tzfbqrt60rv+W1JffUHofvPdFYtypPzJZgtkzldQy9z5x1xai0c65zpeln3rrkVC7pldHZ8mev92qOYndSNRWLcj9pOY/K+VRumH9Zl4D62jrfn626Vwlsz6xupi+vz1nTV0bXFkuA+0CSZ9QaXvfq+vlV7uMrI2iPVvc3npBkeZK/q17JUW6L6QVh5Vz605pjuV2o/JtQpvmC4Vp2swQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABAgQIECAwTgL/HyupltfRCJH4AAAAAElFTkSuQmCC"
    }
   },
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![image.png](attachment:image.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"JAY09\"></a>[STR19]\n",
    "        </td>\n",
    "        <td>\n",
    "            Jaynes, E. T. (2007), \"Probability Theory: The logic of science (5 ed.)\", Cambridge University Press, 2007.\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "This exercise covered some fundamentals of statistical estimators."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "Estimators: Fundamentals and Exercises <br/>\n",
    "by Christian Herta <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2019 Christian Herta\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
